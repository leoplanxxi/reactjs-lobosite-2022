import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css'
import styles from '../styles/styles.scss'
import '../globals'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
