import Header from './Components/Header.js'
import Menu from './Components/Menu.js'
import Slider_Principal from './Components/Homepage/SliderPrincipal.js'
import Buscador from './Components/Homepage/Buscador.js'
import Slider_Intermedio from './Components/Homepage/SliderIntermedio.js'
import Noticias from './Components/Homepage/Noticias.js'
import Video from './Components/Homepage/Video.js'
import Eventos from './Components/Homepage/Eventos.js'
import Slider_Avisos from './Components/Homepage/SliderAvisos.js'
import Convocatorias from './Components/Homepage/Convocatorias.js'
import Accordion_Principal from './Components/Homepage/AccordionPrincipal.js'

export default function Home() {
  return (
  	<>
    <div className={"container container-home"}>
   		<Header />
		<Menu />
    	<Slider_Principal />
    	<Buscador />
    	<Slider_Intermedio />
    	<Noticias />
    	<Video />
    	<Accordion_Principal />
    </div>
    <div className={"container-fluid container-parallax-home"}>
    	<Eventos />
    	<Slider_Avisos />
    	<Convocatorias />
    </div>
    </>
  )
}
