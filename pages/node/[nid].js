import Header from '../Components/Header.js'
import { useRouter } from 'next/router'
import { useEffect, useState } from "react"

const Node = () => {
	const router = useRouter()
	const { nid } = router.query

	const [data, setData] = useState([])

    useEffect(() => {
        fetch(global.config.variables.url_base+"ws_node/"+router.query["nid"])
            .then((res) => res.json())
            .then((data) => {
                setData(data)
                console.log(data)
            })
        console.log(data)
    },[])
    
	return(
		<>
		<Header />
		<h1 className={"text-center"}>{data[0].title}</h1>
		<div dangerouslySetInnerHTML={{__html: data[0].body}}></div>
		</>
	)		
}

export default Node
