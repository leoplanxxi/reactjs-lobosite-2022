import React,{Component} from 'react';
import escudo from '../Assets/img/escudo_blanco.png';

class Header extends Component {
	render() {
		return (
			<div className={"container-fluid container-header"}>
				<div className={"row"}>
					<div className={"col-12"}>
						<img src={escudo.src} />
					</div>
				</div>
			</div>	
		);
	}
}

export default Header;
