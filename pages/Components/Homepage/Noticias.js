import React, {Component} from 'react';

/*json.forEach(function(e){
				url_img = e.field_imagen_slider_principal[0].url;	
			});*/
class Noticias extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.boletines_url)
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;
		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div className={"row row-noticias"}>
					<h1 className={"titulo-noticias"}>Noticias</h1>
					<div className={"col-lg-12 col-noticias"}>
						<div className={"row"}>

						{items.nodes.map(item=>(
							<div className={"col-lg-4"}>
								<p className={"noticia-categoria"}>{item.node.categoria}</p>
								<a href={item.node.ruta} target="_blank"><img src={item.node.img.src} className={"img img-fluid"} /></a>
								<p className={"noticia-titulo"}>{item.node.title}</p>
								<p className={"noticia-leer-mas"}><a href={item.node.ruta} target="_blank">Leer más</a></p>
							</div>			
						))}
						</div>
						<div className={"row"}>
							<a href="https://www.boletin.buap.mx/?q=boletines" target="_blank" className={"ver-noticias"}>Ver todas las noticias</a>
						</div>
					</div>
				</div>
			);
		}
	}
}

export default Noticias;
