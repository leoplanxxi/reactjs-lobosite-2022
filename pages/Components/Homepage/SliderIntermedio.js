import React, {Component} from 'react';
import {
	Carousel,
	CarouselControl,
	CarouselIndicators,
	CarouselItem
} from 'reactstrap';
/*json.forEach(function(e){
				url_img = e.field_imagen_slider_principal[0].url;	
			});*/
class SliderIntermedio extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "ws_slider_intermedio")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;

		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div className={"row row-slider-intermedio"}>
					<div className={"col-lg-12 col-slider-intermedio"}>
						<Carousel
							activeIndex={0}
							next={function noRefCheck() {}}
							prev={function noRefCheck() {}}
						>
							{items.map(item=>(
								<CarouselItem
									onExited={function noRefCheck() {}}
									onExiting={function noRefCheck() {}}
								>
									{ item.vinculo_slider != "" ? (
										<a href={item.vinculo_slider}><img src={global.config.variables.site_url_base+item.imagen_slider} className={"img img-fluid"} /></a>
									): ( <img src={global.config.variables.site_url_base+item.imagen_slider} className={"img img-fluid"} /> )}
									
								</CarouselItem>
							))}
							<CarouselControl
								direction="prev"
								directionText="Anterior"
								onClickHandler={function noRefCheck() {}} />
							<CarouselControl
								direction="next"
								directionText="Siguiente"
								onClickHandler={function noRefCheck() {}} />
						</Carousel>
					</div>
				</div>
			);
		}
	}
}

export default SliderIntermedio;
