import React, {Component} from 'react';
import {
	Accordion,
	AccordionItem,
	AccordionHeader,
	AccordionBody
} from 'reactstrap';

class Accordion_Principal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "ws_slider_principal")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;
		console.log(items)
		
		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div className={"row row-accr-principal"}>
					<div className={"col-lg-12 col-accr-principal"}>
					<div>
					  <Accordion
					    open="0"
					    toggle={function noRefCheck(){}}
					  >
					  {items.map((item,key)=>(
					    <AccordionItem>
					      <AccordionHeader targetId={"" + key}>
					       	{ item.titulo }
					      </AccordionHeader>
					      <AccordionBody accordionId={"" + key}>
					 		<img src={global.config.variables.site_url_base+item.imagen_slider} className={"img img-fluid"} />
					      </AccordionBody>
					    </AccordionItem>					  	
					  ))}
					  </Accordion>
					</div>

						  
				  </div>
				</div>
					
			);
		}
	}
}

export default Accordion_Principal;
