import React, {Component} from 'react';

class Convocatorias extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "ws_convocatorias")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;

		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div className={"row row-convocatorias"}>
					<div className={"col-lg-12 col-convocatorias"}>	
						<div className={"row"}>			
						{items.map(item=>(
							<a href="#" className={"col-lg-6 col-convocatoria"}>
								{item.title}
							</a>
						))}
						</div>
					</div>
				</div>
			);
		}
	}
}

export default Convocatorias;
