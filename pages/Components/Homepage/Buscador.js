import React, {Component} from 'react';

class Buscador extends Component {
	render() {
		return(
			<div className={"row row-buscador"}>
				<div className={"col-lg-12 col-buscador"}>
					<input type="text" />
					<input type="button" value="Buscar" placeholder="Buscar en BUAP" />
				</div>
			</div>
		)
	}
}

export default Buscador;
