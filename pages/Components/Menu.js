import React,{Component} from 'react'
import { useEffect, useState } from "react"

class Menu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "api/menu_items/main")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);
	}

	render() {
		const { error, isLoaded, items} = this.state;
        console.log(items)

		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div className={"row row-menu"}>
					<div className={"col-lg-12 col-menu"}>
                        <nav className={"navbar navbar-expand-lg navbar-light bg-light"}>
                            <div className={"container-fluid"}>
                                <a className={"navbar-brand"} href="#">&nbsp;</a>
                                <button className={"navbar-toggler"} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className={"navbar-toggler-icon"}></span>
                                </button>
                                <div className={"collapse navbar-collapse"} id="navbarNav">
                                    <ul className={"navbar-nav"}>
                                        {items.map(item=>(  
                                            <>
                                                { item.below != undefined ? ( 
                                                <li className={"nav-item dropdown"}>
                                                    <a className={"nav-link dropdown-toggle"} href="#" role="button" id="navbarDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">{item.title}</a>
                                                    <ul className={"dropdown-menu"} aria-labelledby="navbarDropdownMenuLink">
                                                        {(item.below).map(subitem=>(
                                                            <li className={"nav-item"}>
                                                                <a className={"nav-link"} href={subitem.relative}>{subitem.title}</a>
                                                            </li>
                                                        ))}
                                                    </ul>
                                                </li>
                                                ):(
                                                <li className={"nav-item"}>
                                                    <a className={"nav-link"} href={item.relative}>{item.title}</a>
                                                </li>)}
                                                
                                            </>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </nav>
					</div>
				</div>
			);
		}
	}
}

export default Menu;
