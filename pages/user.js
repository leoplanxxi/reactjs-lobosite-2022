/* Fuente: https://drupalchamp.org/blog/user-login-rest-api-drupal8 */
import { useEffect, useState } from "react"

const User = () => {

    const [token, setToken] = useState("")
	const [data, setData] = useState([])
    const [loggedIn, setLoggedIn] = useState(false)

    useEffect(() => {
        fetch(global.config.variables.url_base+"session/token", {method: 'GET'})
            .then((res) => res.text())
            .then((token) => {
                setToken(token)
            })
        console.log("Token: " + token)

        fetch(global.config.variables.url_base+"user/login?_format=json", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': token,
            },
            body: JSON.stringify({
                'name':'test',
                'pass':'test',
            }),
        })
        .then(response => response.json())
        .then(data => {
            console.log('Login response: ',data);
            if (data["current_user"] != undefined) setLoggedIn(true)
            else setLoggedIn(false)
        })
    },[])
    
	return(
		<>
		{ loggedIn ? (<p>Sesión iniciada</p>):(<p>Usuario o contraseña incorrecto</p>)}
		</>
	)		
}
export default User